<?php

defined('_JEXEC') or die();

use JDate;

//$this->addJavascriptFile('media://com_hbookings/js/zabuto_calendar.min.js');
//$this->addCssFile('media://com_hbookings/css/zabuto_calendar.min.css');

$startday = date('w', strtotime($this->year . '-' . $this->month . '-01'));
$countdays = date('t', strtotime($this->year . '-' . $this->month . '-01'));
$month = date('F', strtotime($this->year . '-' . $this->month . '-01'));

if($startday == 0) {
	$startday = 7;
}

if ($this->month == 1) {
	$prevmonth = 12;
	$prevyear = $this->year-1;
} else {
	$prevmonth = $this->month-1;
	$prevyear = $this->year;
}
if ($this->month == 12) {
	$nextmonth = 1;
	$nextyear = $this->year+1;
} else {
	$nextmonth = $this->month+1;
	$nextyear = $this->year;
}

?>

<h1><?php echo $this->item->title; ?></h1>
<div class="hbookingsBooking">
	<a href="<?php echo JRoute::_('index.php?view=booking&task=add') ?>"><button class="hbookingsButtonBooking"><?php echo JText::_('COM_HBOOKINGS_RESSOURCE_BOOKING'); ?></button></a>
</div>
<?php
	echo $this->item->location;
?>

<div class="month">
	<ul>
		<li class="prev"><a href="<?php echo JRoute::_('index.php?view=ressource&month='. $prevmonth .'&year='. $prevyear .'&id='. $this->item->hbookings_ressource_id); ?>">&lt;</a></li>
		<li class="next"><a href="<?php echo JRoute::_('index.php?view=ressource&month='. $nextmonth .'&year='. $nextyear .'&id='. $this->item->hbookings_ressource_id); ?>">&gt;</a></li>
		<li style="text-align:center">
		<?php echo JText::_($month); ?><br>
		<span style="font-size:18px"><?php echo $this->year; ?></span>
		</li>
	</ul>
</div>

<ul class="weekdays">
	<li><?php echo Jtext::_('COM_HBOOKINGS_MO'); ?></li>
	<li><?php echo Jtext::_('COM_HBOOKINGS_TU'); ?></li>
	<li><?php echo Jtext::_('COM_HBOOKINGS_WE'); ?></li>
	<li><?php echo Jtext::_('COM_HBOOKINGS_TH'); ?></li>
	<li><?php echo Jtext::_('COM_HBOOKINGS_FR'); ?></li>
	<li><?php echo Jtext::_('COM_HBOOKINGS_SA'); ?></li>
	<li><?php echo Jtext::_('COM_HBOOKINGS_SU'); ?></li>
</ul>

<ul class="days">
	<?php
	for ($i = 1; $i < $startday; $i++) {
		echo "<li></li>\n";
	}
	for ($i = 1; $i <= $countdays; $i++) {
		
		$date = new JDate($this->year . '-' . $this->month . '-' . $i);
		$dateUnix = $date->toUnix();
		$spanclass = "available";
		
		foreach($this->item->bookings as $booking) {
			
			$jFrom = new JDate($booking->startdate);
			$from = $jFrom->toUnix();
			$jTo = new JDate($booking->enddate);
			$to = $jTo->toUnix();
			
			if($dateUnix >= $from AND $dateUnix <= $to) {
				if($booking->confirmation == 1) {
					$spanclass = "booked";
				} else {
					$spanclass = "pending";
				}
			}
		}
		echo "<li><span class=\"". $spanclass ."\">";
		if($i < 10) {
			echo "0";
		}
		echo $i . "</span></li>\n";
	}
	?>
</ul>