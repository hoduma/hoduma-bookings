<?php

namespace Hoduma\Hbookings\Site\Controller;

defined('_JEXEC') or die();

use FOF30\Container\Container;
use FOF30\Controller\DataController;

class Bookings extends DataController
{
	protected function onBeforeBrowse()
	{
		$user = $this->container->platform->getUser();
		
		$isAdmin = $user->authorise('core.manage', 'com_hbookings');
		if(!$isAdmin) {
			\JFactory::getApplication()->redirect(\JUri::base() . 'index.php?option=com_hbookings&view=ressources');
		}
	}
}